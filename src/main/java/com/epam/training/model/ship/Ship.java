package com.epam.training.model.ship;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Ship implements Serializable {
    private List<Droid> droids;

    public Ship() {
        droids = new ArrayList<>();
    }

}
