package com.epam.training.model.ship;

import java.util.List;

public class ShipBusinessLogic implements ShipModel {
    private Ship ship;

    public ShipBusinessLogic() {
        ship = new Ship();
    }

    @Override
    public boolean addDroid(String name, int model, boolean canMove) {
        List<Droid> droidList = ship.getDroids();
        return droidList.add(new Droid(name, model, canMove));
    }

    @Override
    public boolean removeDroid(int index){
        List<Droid> droidList = ship.getDroids();
        Droid droid = droidList.get(index);
        return droidList.remove(droid);
    }

    @Override
    public List<Droid> getDroidsOnShip() {
        return ship.getDroids();
    }

    @Override
    public Ship getShip() {
        return ship;
    }
}
