package com.epam.training.model.ship;

import java.util.List;

public interface ShipModel {
    boolean addDroid(String name, int model, boolean canMove);
    boolean removeDroid(int index);
    List<Droid> getDroidsOnShip();
    Ship getShip();
}
