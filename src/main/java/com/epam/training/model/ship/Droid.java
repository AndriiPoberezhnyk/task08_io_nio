package com.epam.training.model.ship;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Random;
import java.util.ResourceBundle;

@Getter
@Setter
public class Droid implements Serializable {
    private transient ResourceBundle bundle;
    private String name;
    private int model;
    private transient int speed;
    private transient boolean canMove;

    public Droid(String name, int model, boolean canMove) {
        bundle = ResourceBundle.getBundle("values");
        this.name = name;
        this.model = model;
        this.canMove = canMove;
        if (canMove) speed = generateParameter();
        else speed = 0;
    }

    private int generateParameter(){
        int bound = Integer.parseInt(bundle.getString("SpeedBounds"));
        return 1 + new Random().nextInt(bound);
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", model=" + model +
                ", speed=" + speed +
                ", canMove=" + canMove +
                '}';
    }
}
