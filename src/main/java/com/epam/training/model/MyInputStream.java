package com.epam.training.model;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyInputStream extends InputStream {
    private volatile InputStream in;
    private boolean isUnreadCalled;
    private int data;

    public MyInputStream(InputStream in) {
        this.in = in;
        isUnreadCalled = false;
    }

    @Override
    public int read() throws IOException {
        if (isUnreadCalled)  isUnreadCalled = false;
        else data = in.read();
        return data;
    }

    public void close() throws IOException {
        in.close();
    }

    public void unread() throws IOException {
        isUnreadCalled = true;
    }


}
