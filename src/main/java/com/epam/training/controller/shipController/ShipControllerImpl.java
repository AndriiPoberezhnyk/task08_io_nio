package com.epam.training.controller.shipController;

import com.epam.training.model.ship.Droid;
import com.epam.training.model.ship.ShipBusinessLogic;
import com.epam.training.model.ship.ShipModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;


public class ShipControllerImpl implements ShipController {
    private static final Logger logger = LogManager.getLogger(ShipControllerImpl.class.getName());
    private ShipModel ship;

    public ShipControllerImpl() {
        ship = new ShipBusinessLogic();
    }

    @Override
    public List<Droid> getDroidsOnShip() {
        return ship.getDroidsOnShip();
    }

    @Override
    public boolean addDroid(String name, int model, boolean canMove) {
        return ship.addDroid(name,model,canMove);
    }

    @Override
    public boolean removeDroid(int index) {
        return ship.removeDroid(index);
    }

    @Override
    public void serializeShip() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("droids.ship"));
        ship.getDroidsOnShip().forEach(droid -> {
            logger.info(droid);
            try {
                out.writeObject(droid);
            } catch (IOException e) {
                logger.warn("object wasn't written = " + droid + e.getCause());
            }
        });
        out.close();
    }

    @Override
    public void readDroidsFromFile() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(
                new FileInputStream("droids.ship"));
        boolean isNotNull = true;
        do {
            Droid droidFromFile = (Droid)in.readObject();
            if (droidFromFile == null) isNotNull = false;
            else logger.info(droidFromFile);
        }while (isNotNull);
    }

}
