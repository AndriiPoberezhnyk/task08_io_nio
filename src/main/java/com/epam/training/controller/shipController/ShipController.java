package com.epam.training.controller.shipController;

import com.epam.training.model.ship.Droid;

import java.io.IOException;
import java.util.List;

public interface ShipController {
    List<Droid> getDroidsOnShip();
    boolean addDroid(String name, int model, boolean canMove);
    boolean removeDroid(int index);
    void serializeShip() throws IOException;
    void readDroidsFromFile() throws IOException, ClassNotFoundException;
}
