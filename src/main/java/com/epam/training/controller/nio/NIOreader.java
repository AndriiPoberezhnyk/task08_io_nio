package com.epam.training.controller.nio;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ResourceBundle;

public class NIOreader {
    private static final Logger logger = LogManager.getLogger(NIOreader.class.getName());
    private RandomAccessFile aFile;
    private FileChannel inChannel;
    private ByteBuffer buf;
    private ResourceBundle properties;

    public NIOreader(File file) throws IOException {
        properties = ResourceBundle.getBundle("values");
        try {
            aFile = new RandomAccessFile(file, "rw");
            inChannel = aFile.getChannel();
            buf = ByteBuffer.allocate(Integer.parseInt(properties.getString(
                    "BufferSize")));
        } catch (FileNotFoundException ignored) {
        }
    }

    public void readFile(ByteBuffer buf) throws IOException {
        int bytesRead = inChannel.read(buf);
        while (bytesRead != -1) {
            logger.info("read bytes -> " + bytesRead);
            buf.flip();
            while(buf.hasRemaining()){
                logger.info("remains" + (char) buf.get());
            }
            buf.clear();
            bytesRead = inChannel.read(buf);
        }
    }
}
