package com.epam.training.view;

import com.epam.training.controller.shipController.ShipController;
import com.epam.training.controller.shipController.ShipControllerImpl;
import com.epam.training.model.MyInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private Locale locale;
    private ResourceBundle bundle;
    private ResourceBundle properties;
    private ShipController shipController;

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        properties = ResourceBundle.getBundle("values");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        shipController = new ShipControllerImpl();
    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
                logger.error(e);
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::i18nEnglish);
        methodsMenu.put("2", this::i18nUkrainian);
        methodsMenu.put("3", this::callShipMenu);
        methodsMenu.put("4", this::pdfReading);
        methodsMenu.put("5", this::checkMyInputStream);
        methodsMenu.put("6", this::callDirectoryMenu);
        outputMenu();
    }

    private void i18nEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu");
    }

    private void i18nUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
    }

    private void callShipMenu(){
        ShipView shipView = new ShipView(locale, shipController);
        String keyMenu;
        do {
            menu = shipView.getMenu();
            methodsMenu = shipView.getMethodsMenu();
            outputMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("B"));
    }

    private void pdfReading(){
        LocalTime localTime;
        LocalTime timeAfterReading = null;
        int bufferSize = Integer.parseInt(properties.getString(
                "BufferSize"));
        try {
            DataInputStream inputStream =
                    new DataInputStream(
                            new FileInputStream(
                                    properties.getString("PdfFile")));
            localTime = LocalTime.now();
            readData(inputStream);
            inputStream.close();
            timeAfterReading = LocalTime.now();
            logger.info("Reading one by one finished ");
            logger.info("Started at " + localTime);
            logger.info("Finished at " + timeAfterReading);
        } catch (FileNotFoundException e) {
            logger.error("File doesn't exist");
        } catch (IOException ignored) {
        }
        try {
            DataInputStream bufferedStream =
                    new DataInputStream(
                        new BufferedInputStream(
                             new FileInputStream(
                                     properties.getString("PdfFile")),
                                     bufferSize));
            localTime = LocalTime.now();
            readData(bufferedStream);
            bufferedStream.close();
            timeAfterReading = LocalTime.now();
            logger.info("Buffered reading finished ");
            logger.info("Buffer size " + bufferSize);
            logger.info("Started at " + localTime);
            logger.info("Finished at " + timeAfterReading);
        } catch (FileNotFoundException e) {
            logger.error("File doesn't exist");
        } catch (IOException ignored) {
        }
    }

    private void checkMyInputStream(){
        try {
            MyInputStream myInputStream = new MyInputStream(
                    new FileInputStream(
                            properties.getString("PdfFile")));
            logger.info("read " + myInputStream.read());
            logger.info("read " + myInputStream.read());
            myInputStream.unread();
            logger.info("push back");
            logger.info("read " + myInputStream.read());
        } catch (FileNotFoundException e) {
            logger.error("File doesn't exist");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callDirectoryMenu(){
        DirectoryView directoryView = new DirectoryView(locale);
        String keyMenu;
        do {
            menu = directoryView.getMenu();
            methodsMenu = directoryView.getMethodsMenu();
            System.out.println();
            logger.info("Current directory -> " + directoryView.getCurrentDir().toString());
            outputMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
                logger.error(e);
            }
        } while (!keyMenu.equals("B"));
    }



    //----------------------------------------------
    private void readData(DataInputStream inputStream){
        int countBytes = 0;
        try {
            int data = inputStream.read();
            while (data != -1){
                countBytes++;
                data = inputStream.read();
            }
        } catch (EOFException e){
            logger.info("Unexpected end of file");
            logger.info("Counted bytes = " + countBytes);
            return;
        } catch (IOException e) {
            logger.info("Unexpected IOException");
            logger.info("Counted bytes = " + countBytes);
            return;
        }
        logger.info("End of file");
        logger.info("Counted bytes = " + countBytes);
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
