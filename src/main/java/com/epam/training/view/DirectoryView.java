package com.epam.training.view;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

@Getter
class DirectoryView {
    private static final Logger logger = LogManager.getLogger(DirectoryView.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private StringBuilder previousDir;
    private StringBuilder currentDir;
    private ResourceBundle bundle;
    private ResourceBundle properties;

    DirectoryView(Locale locale) {
        bundle = ResourceBundle.getBundle("DirectorySearchMenu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        properties = ResourceBundle.getBundle("values");
        generateMenu();
        previousDir = new StringBuilder();
        currentDir = new StringBuilder();
        currentDir.append(properties.getString("SourceDirectory"));
    }

    private void generateMenu() {
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("B", bundle.getString("B"));

        methodsMenu.put("1", this::showFilesInDirectory);
        methodsMenu.put("2", this::changeDirectory);
        methodsMenu.put("3", this::movePreviousDirectory);
    }

    private void showFilesInDirectory(){
        File directory = new File(currentDir.toString());
        if (!directory.exists()) {
            logger.error("directory doesn't exist");
        }else{
            File[] files = new File(currentDir.toString())
                    .listFiles(File::isFile);
            File[] dirs = new File(currentDir.toString())
                    .listFiles(File::isDirectory);
            if (dirs!=null)
                Arrays.stream(dirs).forEach(dir ->
                        logger.info("Directory -> " + dir.getName()));
            if (files!=null)
                Arrays.stream(files).forEach(file ->
                        logger.info("File -> " + file.getName()));
        }
    }

    private void changeDirectory(){
        previousDir = new StringBuilder(currentDir);
        currentDir.append(scanner.nextLine());
        while (!checkIfDirectoryExist(currentDir.toString())){
            currentDir = new StringBuilder(previousDir.toString());
            logger.warn("Wrong directory name, enter again");
            currentDir.append(scanner.nextLine());
        }
    }

    private void movePreviousDirectory(){
        if (previousDir.toString().length() > 0)
            currentDir = new StringBuilder(previousDir);
        else {
            currentDir = new StringBuilder(
                    properties.getString("SourceDirectory"));
            logger.warn("Can't move back, there is no previous dir");
        }
    }

    private boolean checkIfDirectoryExist(String dirName){
        return new File(dirName).exists();
    }
}
