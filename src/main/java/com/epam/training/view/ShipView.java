package com.epam.training.view;

import com.epam.training.controller.shipController.ShipController;
import com.epam.training.controller.shipController.ShipControllerImpl;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.EOFException;
import java.io.IOException;
import java.util.*;

@Getter
class ShipView {
    private static final Logger logger = LogManager.getLogger(ShipView.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private ResourceBundle bundle;
    ShipController shipController;

    ShipView(Locale locale, ShipController shipController) {
        bundle = ResourceBundle.getBundle("ShipMenu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        this.shipController = shipController;
        fillDroidList();
        generateMenu();
    }

    private void generateMenu() {
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("B", bundle.getString("B"));

        methodsMenu.put("1", this::showListOfDroids);
        methodsMenu.put("2", this::serialize);
        methodsMenu.put("3", this::deserialize);
    }

    private void showListOfDroids(){
        shipController.getDroidsOnShip().forEach(logger::info);
    }

    private void serialize(){
        try {
            shipController.serializeShip();
        } catch (IOException e) {
            logger.warn("Exception while serialize ship");
        }
    }

    private void deserialize(){
        try {
            shipController.readDroidsFromFile();
        } catch (EOFException e){
            logger.warn("End of file");
        } catch (IOException e) {
            logger.warn("IOException while deserialize ship");
        } catch (ClassNotFoundException e) {
            logger.warn("Class not found exception while deserialize ship");
        }
    }

    private void fillDroidList(){
        shipController.addDroid("F", 1, true);
        shipController.addDroid("F", 2, true);
        shipController.addDroid("F", 2, false);
        shipController.addDroid("F", 2, true);
        shipController.addDroid("F", 3, true);
    }
}
